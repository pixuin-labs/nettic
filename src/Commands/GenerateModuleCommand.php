<?php

namespace PixuinLabs\Nettic\Commands;



use Nette\Caching\Storages\FileStorage;
use Nette\DI\Container;
use Nette\DI\ContainerBuilder;
use Nette\PhpGenerator\ClassType;
use Nette\Utils\Strings;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateModuleCommand extends Command
{
    /**
     * @var Container
     */
    protected $container;

    protected function configure(): void
    {
        $this->setName('module:create')
            ->addArgument('module-name')
            ->setDescription('Create module');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {

            /** @var Container $container */
            $this->container = $this->getHelper('container')->getByType(Container::class);
            $moduleName = $input->getArgument('module-name');
            mkdir($this->container->parameters['appDir'].'/modules/'.Strings::lower($moduleName));
            mkdir($this->container->parameters['appDir'].'/modules/'.Strings::lower($moduleName).'/presenters');
            mkdir($this->container->parameters['appDir'].'/modules/'.Strings::lower($moduleName).'/presenters/templates');
            $this->createBasePresenter($moduleName);
            $this->createLayout($moduleName);


//            $input->getOption('module');
//            Debugger::dump($this->di->getParameters()['appDir']);
//            $output->writeln();


            $output->writeLn('Module created.');
            return 0; // zero return code means everything is ok

        } catch (\Exception $e) {
            $output->writeLn('<error>' . $e->getMessage() . '</error>');
            return 1; // non-zero return code means error
        }
    }

    protected function createBasePresenter($moduleName)
    {
        $basePresenter = ClassType::from(\BasePresenter::class)
            ->addExtend("\\App\\CoreModule\\Presenters\\BasePresenter")
            ->setAbstract()
        ;
        $basePresenter = sprintf("<?php\n\nnamespace App\\%sModule\\Presenters;\n\n$basePresenter", Strings::firstUpper($moduleName));
        $basePresenter = str_replace('App\CoreModule', '\App\CoreModule', $basePresenter);
        file_put_contents($this->container->parameters['appDir'].'/modules/'.Strings::lower($moduleName).'/presenters/BasePresenter.php', $basePresenter);
    }

    public function createLayout($moduleName) {
        $layout = "{extends ../../../core/presenters/templates/@layout.latte}";
        file_put_contents($this->container->parameters['appDir'].'/modules/'.Strings::lower($moduleName).'/presenters/templates/@layout.latte', $layout);
    }

}