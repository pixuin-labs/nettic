<?php

namespace PixuinLabs\Nettic;


use Nette\DI\CompilerExtension;

class NetticExtension extends CompilerExtension
{

    private $defaults = [

    ];

    public function loadConfiguration()
    {
        $config = $this->validateConfig($this->defaults);
        $this->validateConfig(['decorator' => $this->loadFromFile(__DIR__ . '/config/config.neon')['decorator']]);
        $builder = $this->getContainerBuilder();


        $this->compiler->loadDefinitions(
            $builder,
            $this->loadFromFile(__DIR__ . '/config/config.neon')['services'],
            $this->name
        );
    }
}